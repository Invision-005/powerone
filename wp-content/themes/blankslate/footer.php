<div class="clear"></div>
</div>
<div class="container-fluid footer-part">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12 footer-content1">
				<div class="footer-bg">
					<h1>Subscribe To Our E-mail List</h1>
					<form class="subscriber-form">
						<p class="fname"><input type="text" placeholder="First Name*" required></p>
						<p class="lname"><input type="text" placeholder="Last Name*" required></p>
						<p><input type="email" placeholder="Email Address*" class="email-text" required></p>
						<p class="companyname"><input type="text" placeholder="Company Name"></p>
						<div class="country">
						<div class="country-subpart">
							<img src="<?php bloginfo('template_url');?>/images/canada.png">
							<p>CAN</p>
							<input type="radio" name="can" value="can" checked>
						</div>
						<div class="country-subpart">
							<img src="<?php bloginfo('template_url');?>/images/usa.png">
							<p>USA</p>
							<input type="radio" name="can" value="usa">
						</div>
						<div class="country-subpart">
							<img src="<?php bloginfo('template_url');?>/images/others.png">
							<p>OTHER</p>
							<input type="radio" name="can" value="other">
						</div>
						</div>
						<div class="clear"></div>
						<!--<p class="submit"><input type="submit" value="Submit"></p>-->
						<button class="submit btn primary-btn sm">Submit</button>
					</form>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 footer-content2">
				<h1>Latest News</h1>
				<div class="col-sm-12">
					<h2>June 29,2018</h2>
					<span>POCML 4 Inc. Announces Closing of Increased
						  Financing by Medipharm Labs Inc. For Gross
						  Proceeds of $22,316,000</span>
					<span class="readmore"><a href="#">Read more</a></span>
				</div>
				<div class="col-sm-12">
					<h2>June 21,2018</h2>
					<span>Laramide Resources Closes Over-Subscribed
						  $3.75M Equity Financing</span>
					<span class="readmore"><a href="#">Read more</a></span>
				</div>
				<button class="btn primary-btn sm">More News</button>
			</div>
		</div>
	</div>
</div>
<footer id="footer" role="contentinfo">
<div id="copyright">
	<div class="row">
		<div class="col-md-6 col-sm-12 cright-part1">
			<?php echo '<a href="#">Legal Disclaimer</a> | <a href="#">Privacy Policy</a>&nbsp; ';
			echo sprintf( __( '%1$s %2$s %3$s Capital Markets Limited', 'blankslate' ), date( 'Y' ),'&copy;', esc_html( get_bloginfo( 'name' ) ) );?>
		</div>
		<div class="col-md-6 col-sm-12 cright-part2">
			<span>Designed By</span>
			<img src="<?php bloginfo('template_url');?>/images/catch_adv_logo.png">
		</div>
	</div>
</div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>