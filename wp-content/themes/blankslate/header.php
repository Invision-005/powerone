<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
    crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
	<header id="header" role="banner">
		<section id="branding">
			<div class="container-fluid">
				<div class="header-container">
					<div class="row">
						<div class="col-sm-12 col-md-4 header-part1">
							<a href="#" class="nav-bar"><img src="<?php bloginfo('template_url');?>/images/bars_icon.png"></a>
							<a href="#" class="search-icon"><img src="<?php bloginfo('template_url');?>/images/search.png"></a>
						</div>
						<div class="col-sm-12 col-md-4 header-part2 text-center">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="img-fluid" src="<?php bloginfo('template_url');?>/images/powerone_logo.png"></a>
						</div>
						<div class="col-sm-12 col-md-4 header-part3">
							<div class="float-none float-md-right">
								<div class="header-subpart3">
									<p>Market Information</p>
									<h1>TSX 12675.67  <span>45.35(0.28%)</span></h1>
								</div>
								<button class="subscribe btn primary-btn sm">Subscribe</button>
							<!--<a href="#" class="subscribe-btn">Subscribe</a>-->
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<div class="banner-img">
			<div class="navigation">
				<nav id="menu" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
				</nav>
			</div>
			<h1 class="banner-text">Providing Capital To Emerging<br>Growth Companies Globally</h1>
		</div>

		<div id="search">
		<?php //get_search_form(); ?>
		</div>

	</header>
<div id="container">